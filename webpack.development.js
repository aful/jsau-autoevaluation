'use strict'

const {merge} = require('webpack-merge')
const common = require('./webpack.common.js')

module.exports = merge(common, {
    mode: 'development',
    // Use mode "none" with no minimization to not have the code inlined
    // mode: 'none',
    // optimization: {
    //     minimize: false,
    // },
    // Disable devtool to be able to load the unminized+unoptimized bundle into IE
    //devtool: false,
    output: {
        clean: true, // Clean the output directory before emitting
    },
})
