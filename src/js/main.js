import {
    getAnswers,
    storeAnswers,
    exportAnswers,
    importAnswers,
} from './lib/import-export.js'

import {getProjectsIds, getProjectCheckpoints} from './lib/projects.js'

const RESULT_ADAPTATION_FACTOR = 22 / 100
const RESULT_LOCALE = 'fr-FR'
const number_format = new Intl.NumberFormat(RESULT_LOCALE)

insertAllProjectsQuestionsHtml()

const answers_input_elems = Array.from(document.querySelectorAll('.answer_input'))
const result_value_elem = document.getElementById('result-value')
const import_file_elem = document.getElementById('import_file')
const import_button_elem = document.getElementById('import-button')
import_button_elem.addEventListener('click', async(evt) => {
    evt.preventDefault()
    // There is always only 1 file to import
    const file = import_file_elem.files[0]
    const answers = await importAnswers(file)
    loadAnswers(answers)
})
const export_button_elem = document.getElementById('export-button')
export_button_elem.addEventListener('click', evt => {
    evt.preventDefault()
    exportAnswers()
})

const toggle_all_button_elem = document.getElementById('toggle-all-button')
toggle_all_button_elem.addEventListener('click', toggleAllCheckboxes)

// TODO: Do some renaming here: →getAnswersFromLocalStorage
loadAnswers(getAnswers())

function insertAllProjectsQuestionsHtml() {
    const projects_ids = getProjectsIds()
    projects_ids.forEach((project_id, idx) => {
        insertProjectQuestionsHtml(project_id, idx + 1)
    })
}

function insertProjectQuestionsHtml(project_id, project_number) {
    const questions = getProjectCheckpoints(project_id)

    const sections_elem = document.getElementById('sections')
    const section_elem = document.createElement('section')
    section_elem.setAttribute('id', `${project_id}-form`)
    sections_elem.appendChild(section_elem)

    const heading_elem = document.createElement('h2')
    heading_elem.innerHTML = `${project_number}. ${project_id}`
    section_elem.appendChild(heading_elem)

    const table_elem = document.createElement('table')
    const thead_elem = document.createElement('thead')
    thead_elem.innerHTML = `
    <tr>
      <th>N°</th><th>Libellé</th><th>Description</th><th>Points</th><th>Statut</th>
    </tr>
    `
    table_elem.appendChild(thead_elem)
    const tbody_elem = document.createElement('tbody')
    Array.from(questions).forEach((question, idx) => {
        const answer_id = `${project_id}-${question.id}`
        const question_tr_elem = document.createElement('tr')
        if (question.specific) {
            question_tr_elem.className = 'specific'
        }
        question_tr_elem.innerHTML =
            `<td>${project_number}.${idx + 1}</td>`
            + `<td>${question.label}</td>`
            + `<td>${question.description}</td>`
            + `<td class="centered">${question.points}</td>`

        const answer_td_elem = document.createElement('td')
        const answer_input_elem = document.createElement('input')
        Object.assign(answer_input_elem, {
            type: 'checkbox',
            name: answer_id,
            id: answer_id,
            className: 'answer_input',
        })
        answer_input_elem.setAttribute('data-points', question.points)
        answer_input_elem.addEventListener('click', (evt) => {
            updateResultDisplay()
            storeAnswersInputElems(answers_input_elems)
        })
        answer_td_elem.appendChild(answer_input_elem)
        const answer_label_elem = document.createElement('label')
        answer_label_elem.setAttribute('for', answer_id)
        answer_label_elem.appendChild(document.createTextNode('Fait'))
        answer_td_elem.appendChild(answer_label_elem)
        question_tr_elem.appendChild(answer_td_elem)

        tbody_elem.appendChild(question_tr_elem)
    })
    table_elem.appendChild(tbody_elem)
    section_elem.appendChild(table_elem)
}

function toggleAllCheckboxes(evt) {
    evt.preventDefault()
    const elems = Array.from(document.querySelectorAll('.answer_input'))
    const all_checked = elems.every((elem) => {
        return elem.checked
    })
    elems.forEach((elem) => {
        elem.checked = !all_checked
    })
    updateResultDisplay()
    storeAnswersInputElems(answers_input_elems)
}

function updateResultDisplay() {
    const result_raw = answers_input_elems.reduce((prev, answer_input_elem) => {
        const points = Number(answer_input_elem.dataset.points)
        return prev + (answer_input_elem.checked && points || 0)
    }, 0)
    const result_adapted = result_raw * RESULT_ADAPTATION_FACTOR
    const result_formatted = number_format.format(result_adapted)
    result_value_elem.textContent = result_formatted
}

function loadAnswers(answers) {
    Object.entries(answers).forEach((answer) => {
        const question_id = answer[0]
        const answer_input_elem = document.getElementById(question_id)

        // The answers previously saved don't match the current answer_input_elem
        if (!answer_input_elem) {
            // eslint-disable-next-line no-console -- Legitimate debug output (infrequent)
            console.log(`No question with id = ${question_id}`)
            return
        }

        answer_input_elem.checked = answer[1].checked
    })

    updateResultDisplay()
}

function storeAnswersInputElems(input_elems) {
    const answers = Object.fromEntries(input_elems.map((elem) => {
        return [elem.id, {checked: elem.checked}]
    }))
    storeAnswers(answers)
}
