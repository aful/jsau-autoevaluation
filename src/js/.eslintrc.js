
module.exports = {
    extends: 'usecases/usecase/browser-modern',
    root: true,
    parserOptions: {
        ecmaVersion: 2017,
        sourceType: 'module'
    },
}
