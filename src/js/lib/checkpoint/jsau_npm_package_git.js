import questions_common from './common.js'

const questions = [
    {
        id: 'export_function_class_property',
        label: 'Export de fonction/classe/propriété',
        description: 'Export d’une ou plusieurs <code>function</code> et/ou <code>class</code> et/ou propriété qui pourront être utilisées dans les autres projets.',
        points: 2,
        specific: true,
    },
    {
        id: 'export_custom_error_type',
        label: 'Export de type d’erreur personnalisé',
        description: 'Export d’une ou plusieurs <code>Error</code> qui pourront être utilisées dans les autres projets, avec <code>if (err instanceof CustomError)</code>, cf. <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error#custom_error_types">Custom Error Types</a>.',
        points: 2,
        specific: true,
    },
    {
        id: 'export_graphic_resource',
        label: 'Export de ressource graphique',
        description: 'Export d’une ou plusieurs ressources graphiques (image, fonte, CSS) qui pourront être utilisées dans les autres projets.',
        points: 2,
        specific: true,
    },
]

export default questions_common.concat(questions)
