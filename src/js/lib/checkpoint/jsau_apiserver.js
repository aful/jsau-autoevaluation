import questions_common from './common.js'

const questions = [
    {
        id: 'npm_start',
        label: 'npm start',
        description: 'La commande <code>npm start</code> démarre l’application.',
        points: 1,
        specific: true,
    },
    {
        id: 'async_callback',
        label: 'Callback',
        description: 'Utilisation dans le code d’un <em>callback normalisé Node.js</em>, identifiée par un commentaire spécial placé à la ligne précédente :<br/><code>// async-callback</code>',
        points: 2,
        specific: true,
    },
    {
        id: 'async_promise_then',
        label: 'Promise then',
        description: 'Utilisation dans le code d’une <em>Promise</em> avec <code>then</code>, identifiée par un commentaire spécial placé à la ligne précédente :<br/><code>// async-promise-then</code>',
        points: 2,
        specific: true,
    },
    {
        id: 'async_promise_async_await',
        label: 'Promise async/await',
        description: 'Utilisation dans le code d’au moins une <em>Promise</em> avec <code>async/await</code>, identifiée par un commentaire spécial placé à la ligne précédente :<br/><code>// async-promise-async-await</code>',
        points: 2,
        specific: true,
    },
    {
        id: 'cache_control',
        label: 'Cache-Control',
        description: 'Chaque réponse de l’application a une entête <code>Cache-Control</code>.',
        points: 1,
        specific: true,
    },
    {
        id: 'ci_test',
        label: 'CI tests',
        description: `
<ul>
<li><code>npm run lint</code></li>
<li>Comme cadre d'exécution des tests (test runner) utiliser <a href="https://mochajs.org/">Mocha</a> +<a href="https://www.npmjs.com/package/nyc">Nyc</a> ou <a href="https://jestjs.io/">Jest</a>
<li>Du contenu avec le content-type <code>application/json</code> doit être
 envoyé au serveur avec le paquet <a
 href="https://www.npmjs.com/package/supertest">supertest</a> et lorsque le serveur renvoie
 du contenu alors le content-type de sa réponse doit être <code>application/json</code></li>
</ul>
`,
        points: 3,
        specific: true,
    },
    {
        id: 'nginx',
        label: 'Intégration avec Nginx',
        description: 'L’application doit être accessible à l’URL <a href="https://web-2.local.test/api/">https://web-2.local.test/api/</a>',
        points: 2,
        specific: true,
    },
    {
        id: 'npm_package_git_private',
        label: 'Paquet NPM Git privé',
        description: 'Utilisation d’un paquet NPM privé dans un dépôt Git.',
        points: 2,
        specific: true,
    },
]

export default questions_common.concat(questions)
