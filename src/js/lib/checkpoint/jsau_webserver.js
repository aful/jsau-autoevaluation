import questions_common from './common.js'

const questions = [
    {
        id: 'npm_start',
        label: 'npm start',
        description: 'La commande <code>npm start</code> démarre l’application.',
        points: 1,
        specific: true,
    },
    {
        id: 'npm_run_stylelint',
        label: 'npm run stylelint',
        description: 'Les commandes <code>npm run stylelint</code> et <code>npm run stylelint:fix</code> valident et corrigent les CSS.',
        points: 1,
        specific: true,
    },
    {
        id: 'npm_run_build',
        label: 'npm run build',
        description: 'La commande <code>npm run build</code> construit l’application avec <a href="https://webpack.js.org/">Webpack</a>, notamment les bundles (<code>.js</code>, <code>.css</code>).',
        points: 3,
        specific: true,
    },
    {
        id: 'cache_control',
        label: 'Cache-Control',
        description: 'Chaque réponse de l’application a une entête <code>Cache-Control</code>.',
        points: 1,
        specific: true,
    },
    {
        id: 'ci_integration_test',
        label: 'CI tests',
        description: `
<ul>
<li><code>npm run lint</code></li>
<li><code>npm run stylelint</code></li>
<li>Utiliser <a href="https://jestjs.io/">Jest</a> comme cadre d'exécution et pour le code coverage
<li>Du contenu avec le content-type <code>application/x-www-form-urlencoded</code> doit être
 envoyé au serveur avec le paquet <a
 href="https://www.npmjs.com/package/supertest">supertest</a> et le serveur doit
 répondre en envoyant du HTML
</ul>
`,
        points: 3,
        specific: true,
    },
    {
        id: 'nginx',
        label: 'Intégration avec Nginx',
        description: 'L’application doit être accessible à l’URL <a href="https://web-1.local.test/">https://web-1.local.test/</a>',
        points: 2,
        specific: true,
    },
    {
        id: 'npm_package_git_private',
        label: 'Paquet NPM Git privé',
        description: 'Utilisation d’un paquet NPM privé dans un dépôt Git.',
        points: 2,
        specific: true,
    },
]

export default questions_common.concat(questions)
