// Format:
// A list of objects having a "label", "description", "points"
const questions = [
    {
        id: 'no_recreable_filesin_git',
        label: 'Pas de fichiers recréables dans Git',
        description: 'Aucun fichier recréable automatiquement (<code>node_modules</code>, bundles <code>.js</code> et <code>.css,</code> etc.) ne doit être stocké dans le dépôt Git. <br/><br/><em>Attention : le fichier <code>package-lock.json</code> n’est pas recréable automatiquement car il est généré par une action délibérée du développeur faite à un moment unique dans le temps créant un arbre de dépendances potentiellement différent à chaque fois, et ainsi il doit donc être stocké dans le dépôt Git</em>',
        points: 1,
    },
    {
        id: 'dot_nvmrc',
        label: '.nvmrc',
        description: 'Présence d’un fichier .nvmrc correct à la racine du projet Git.',
        points: 1,
    },
    {
        id: 'npm_run_lint',
        label: 'npm run lint',
        description: 'Les commandes <code>npm run lint</code> et <code>npm run lint:fix</code> valident et corrigent le code JavaScript en utilisant les configurations fournies par le package <a href="https://www.npmjs.com/package/eslint-config-usecases">eslint-config-usecases</a>. De plus, la commande <code>npm run lint</code> doit être exécutée en <code>posttest</code> et en <code>pretest:ci</code>.',
        points: 3,
    },
    {
        id: 'npm_t',
        label: 'npm t',
        description: 'La commande <code>npm t</code> lance les tests qui s’exécutent sans erreur.',
        points: 2,
    },
    {
        id: 'ci_pipeline',
        label: 'CI pipeline passed',
        description: 'Le dernier commit doit avoir déclenché un pipeline de la CI ayant le statut « passed ». Une coche verte visible sur la page du projet est alors affichée.',
        points: 2,
    },
    {
        id: 'ci_badge_pipeline_passed',
        label: 'CI badge pipeline passed',
        description: 'Affichage du badge « pipeline passed » dans le README.md à la racine du projet, comme sur <a href="https://gitlab.com/madarche/oauth2-resource-server-example/">https://gitlab.com/madarche/oauth2-resource-server-example/</a>',
        points: 1,
    },
    {
        id: 'ci_badge_coverage',
        label: 'CI badge coverage',
        description: 'Affichage du badge « coverage » avec le % calculé dans le README.md à la racine du projet, avec une couverture >= 10%, comme sur <a href="https://gitlab.com/madarche/oauth2-resource-server-example/">https://gitlab.com/madarche/oauth2-resource-server-example/</a>.',
        points: 3,
    },
]

export default questions
