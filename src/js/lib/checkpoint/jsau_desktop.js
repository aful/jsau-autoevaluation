import questions_common from './common.js'

const questions = [
    {
        id: 'npm_start',
        label: 'npm start',
        description: 'La commande <code>npm start</code> démarre l’application.',
        points: 1,
        specific: true,
    },
    {
        id: 'fetch',
        label: 'Utilisation de l’API Fetch',
        description: 'Utilisation de l’<a href="https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch">API Fetch</a> pour faire des requêtes au <em>jsau-apiserver</em>.<br/><br/><em>Cette fonctionnalité peut être factorisée dans le paquet NPM privé dans un dépôt Git.</em><br/><br/><em>Ne pas utiliser le paquet NPM axios</em>.',
        points: 2,
        specific: true,
    },
    {
        id: 'npm_package_git_private',
        label: 'Paquet NPM Git privé',
        description: 'Utilisation d’un paquet NPM privé dans un dépôt Git.',
        points: 2,
        specific: true,
    },
    {
        id: 'npm_run_stylelint',
        label: 'npm run stylelint',
        description: 'Les commandes <code>npm run stylelint</code> et <code>npm run stylelint:fix</code> valident et corrigent les CSS.',
        points: 1,
        specific: true,
    },
    {
        id: 'npm_run_build',
        label: 'npm run build',
        description: 'La commande <code>npm run build</code> construit l’application, notamment les bundles (<code>.js</code>, <code>.css</code>).',
        points: 1,
        specific: true,
    },
    {
        id: 'ci_test',
        label: 'CI tests',
        description: `
<ul>
<li><code>npm run lint</code></li>
<li><code>npm run stylelint</code></li>
<li>Utiliser <a href="https://jestjs.io/">Jest</a> comme cadre d'exécution et pour le code coverage
<li>Simuler la partie serveur avec du mock avec le paquet <a href="https://www.npmjs.com/package/nock">nock</a></li>
</ul>
`,
        points: 3,
        specific: true,
    },
]

export default questions_common.concat(questions)
