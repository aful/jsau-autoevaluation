
import checkpoints_jsau_apiserver from './checkpoint/jsau_apiserver.js'
import checkpoints_jsau_webserver from './checkpoint/jsau_webserver.js'
import checkpoints_jsau_webapp from './checkpoint/jsau_webapp.js'
import checkpoints_jsau_desktop from './checkpoint/jsau_desktop.js'
import checkpoints_jsau_npm_package_git from './checkpoint/jsau_npm_package_git.js'

// eslint-disable-next-line no-undef -- TODO: Update eslint conf
const projects_checkpoints_map = new Map([
    ['jsau-apiserver', checkpoints_jsau_apiserver],
    ['jsau-webserver', checkpoints_jsau_webserver],
    ['jsau-webapp', checkpoints_jsau_webapp],
    ['jsau-desktop', checkpoints_jsau_desktop],
    ['jsau-npm_package_git', checkpoints_jsau_npm_package_git],
])

function getProjectsIds() {
    return Array.from(projects_checkpoints_map.keys())
}

function getProjectCheckpoints(project_id) {
    return projects_checkpoints_map.get(project_id)
}

export {
    getProjectsIds,
    getProjectCheckpoints,
}
