import fileDownload from 'js-file-download'

const STORAGE_ID = 'answers'
const EXPORT_FILE_NAME = 'jsau-autoevaluation-export.json'

function getAnswers() {
    const obj_as_json = getAnswersAsJson()
    return JSON.parse(obj_as_json)
}

function storeAnswers(obj) {
    window.localStorage.setItem(STORAGE_ID, JSON.stringify(obj))
}

function exportAnswers() {
    const obj_as_json = getAnswersAsJson()
    fileDownload(obj_as_json, EXPORT_FILE_NAME, 'application/json')
}

async function importAnswers(file) {
    const str = await file.text()
    return JSON.parse(str)
}

// Private
function getAnswersAsJson() {
    return window.localStorage.getItem(STORAGE_ID) || '{}'
}

export {
    getAnswers,
    storeAnswers,
    exportAnswers,
    importAnswers,
}
