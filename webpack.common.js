'use strict'

const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const now = new Date()

module.exports = {
    entry: {
        main: [
            './src/js/main.js',
            './src/css/main.css',
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'src/index.html',
            templateParameters: {
                modified: now.toISOString(),
                modified_human: toIsoDateStr(now),
            },
            xhtml: true,
            hash: true,
            favicon: 'src/img/favicon-64x64.png',
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                keepClosingSlash: true,
                useShortDoctype: false,
            },
        }),
        new MiniCssExtractPlugin(),
    ],
    module: {
        rules: [{
            test: /\.css$/,
            use: [
                MiniCssExtractPlugin.loader,
                'css-loader',
            ],
        }],
    }
}

function toIsoDateStr(date) {
    return `${date.getUTCFullYear()}-${pad(date.getUTCMonth() + 1)}-${pad(date.getUTCDate())}`
}

function pad(number) {
    if (number < 10) {
        return '0' + number
    }
    return number
}
