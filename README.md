jsau-autoevaluation
===================

Utilisation
-----------

Si vous êtes un élève, voici ce que vous devez faire :

1. Vous devez vous rendre sur la page web
   https://aful.gitlab.io/jsau-autoevaluation
2. Sur cette page web vous devez évaluer chacun de vos projets en cochant les
   cases qui correspondent à l’état de votre TP
3. Toujours sur cette page vous devez exporter l’état des cases dans un fichier
   `jsau-autoevaluation-export.json` à l’aide du bouton « Export »
4. Enfin vous devez ajouter le fichier `jsau-autoevaluation-export.json` obtenu
   dans un nouveau dépôt Git que vous devrez créer, qui devra avoir pour nom
   `jsau-autoevaluation-export`, et qui devra être accessible au professeur
5. Vous devrez maintenir ce dépôt Git avec des informations à jour pour
   permettre au professeur de suivre votre progression et au final de vous
   donner une note sur le TP en s’appuyant sur les déclarations de votre fichier
   `jsau-autoevaluation-export.json`


Construction
------------

Cette section ne contient pas d'instructions pour les élèves. Cette section sert
uniquement à expliquer comment construire et déployer cette petite page web. Les
manipulations décrites dans cette section ne sont pas nécessaires pour générer
un fichier d’autoévaluation.

    $ npm ci
    $ npm run build

Le résultat est un ensemble de fichiers générés dans le sous-répertoire `dist/`.
Pour pouvoir utiliser le résultat il faut ouvrir le fichier `dist/index.html`
avec un navigateur web ayant le support de JavaScript activé.

